package com.twuc.webApp.yourTurn;


import com.twuc.webApp.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import outOfScanningScope.OutOfScanningScope;

import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


public class WithoutDependencyTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void initApplicationContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    //2.1
    @Test
    void should_return_bean() {

        WithoutDependency bean = context.getBean(WithoutDependency.class);

        WithoutDependency bean2 = context.getBean(WithoutDependency.class);
        assertNotEquals(bean, bean2);
        assertNotNull(bean);
    }

    @Test
    void should_return_error_when_out_of_package() {
        assertThrows(RuntimeException.class, () -> {
            OutOfScanningScope bean = context.getBean(OutOfScanningScope.class);
        });
    }

    @Test
    void should_return_error_when_create_by_interface() {
        Interface bean = context.getBean((Interface.class));
        assertNotNull(bean);
    }

    @Test
    void should_get_result_given_interface_and_dependent_when_create_object_with_them() {
        SimpleInterface simpleInterface = context.getBean(SimpleInterface.class);

        assertSame(SimpleObject.class, simpleInterface.getClass());
        assertSame("O_o", simpleInterface.getSimpleDependent().getName());
    }

    //2.2
    @Test
    void should_get_result_given_dependent_when_create_object_with_multi_constructor() {
        MultipleConstructor multipleConstructor = context.getBean(MultipleConstructor.class);

        assertSame(Dependent.class, multipleConstructor.getDependent().getClass());
    }

    @Test
    void should_get_result_given_dependent_and_another_dependent_when_create_object_with_them() {
        WithAutowiredMethod withAutowiredMethod = context.getBean(WithAutowiredMethod.class);

        assertSame(Dependent.class, withAutowiredMethod.getDependent().getClass());
        assertSame(AnotherDependent.class, withAutowiredMethod.getAnotherDependent().getClass());
    }

    @Test
    void should_get_xxx_given_a_interface_when_create_multi_objects_with_it() {
        Map<String, InterfaceWithMultipleImpls> myInterfaceMap = context.getBeansOfType(InterfaceWithMultipleImpls.class);

        Optional<InterfaceWithMultipleImpls> implementationAOptional = myInterfaceMap.values().stream().filter(i -> i.getClass().equals(ImplementationA.class)).findFirst();
        Optional<InterfaceWithMultipleImpls> implementationBOptional = myInterfaceMap.values().stream().filter(i -> i.getClass().equals(ImplementationB.class)).findFirst();
        Optional<InterfaceWithMultipleImpls> implementationCOptional = myInterfaceMap.values().stream().filter(i -> i.getClass().equals(ImplementationC.class)).findFirst();
        assertEquals(3, myInterfaceMap.size());
        assertNotNull(implementationAOptional.orElse(null));
        assertNotNull(implementationBOptional.orElse(null));
        assertNotNull(implementationCOptional.orElse(null));
    }
}
