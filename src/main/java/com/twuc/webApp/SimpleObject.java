package com.twuc.webApp;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class SimpleObject implements SimpleInterface {
    private SimpleDependent simpleDependent;

    public SimpleObject(SimpleDependent simpleDependent) {
        this.simpleDependent = simpleDependent;
    }

    public SimpleDependent getSimpleDependent() {
        return simpleDependent;
    }
}
