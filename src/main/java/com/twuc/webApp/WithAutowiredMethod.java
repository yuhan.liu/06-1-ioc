package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {

    private Dependent dependent;
    private AnotherDependent anotherDependent;

    public WithAutowiredMethod(Dependent dependent) {
        this.dependent = dependent;
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        this.anotherDependent = anotherDependent;
    }

    public Dependent getDependent() {
        return dependent;
    }

    public AnotherDependent getAnotherDependent() {
        return anotherDependent;
    }
}

