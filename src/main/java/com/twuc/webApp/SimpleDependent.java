package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SimpleDependent {
    private String name;

    public SimpleDependent() {
    }

    public String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }
}
